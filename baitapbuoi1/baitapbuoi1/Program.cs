﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace baitapbuoi1
{
    internal class Program
    {
        struct ThongTin
        {
            public string HoTen;
            public string NamSinh;
            public string GioiTinh;
            public string QueQuan;
            public string SoCCCD;
        }

        static void Main(string[] args)
        {
            int chon;

            do
            {
                Console.WriteLine("\n ----------MENU----------");
                Console.WriteLine("1. Tim do dai cua chuoi");
                Console.WriteLine("2. Dem so tu trong chuoi");
                Console.WriteLine("3. Dem so nguyen am va phu am trong chuoi");
                Console.WriteLine("4. Tinh tong cac chu so cua mot so nguyen");
                Console.WriteLine("5. Sap xep mang theo thu tu giam dan");
                Console.WriteLine("6. Tinh giai thua cua mot so");
                Console.WriteLine("7. Nhap va xuat thong tin can cuoc cong dan Viet Nam");
                Console.WriteLine("8. Nhap so nguyen duong n và tim n so Fibonacci");
                Console.WriteLine("0. Thoat chuong trinh");
                Console.Write("\n Chon menu bai tap: ");
                chon = int.Parse(Console.ReadLine());
                switch (chon)
                {
                    case 1:
                        Console.Write("Nhap chuoi: ");
                        string str = Console.ReadLine();
                        int length = 0;
                        foreach (char c in str)
                        {
                            length++;
                        }
                        Console.WriteLine("Do dai chuoi: " + length);
                        break;

                    case 2:
                        Console.Write("Nhap chuoi: ");
                        string kytu = Console.ReadLine();

                        //phương thức Split() để chia chuỗi thành một mảng các từ dựa trên khoảng trắng
                        int count = kytu.Split(' ').Length;

                        Console.WriteLine("So tu trong chuoi: " + count);
                        break;

                    case 3:
                        Console.WriteLine("Nhap mot doan ky tu:");
                        string chuoi = Console.ReadLine();
                        int nguyenam = 0, phuam = 0;

                        //chuyển chuỗi thành chữ thường bằng phương thức ToLower() để dễ dàng kiểm tra từng ký tự.
                        str = chuoi.ToLower();

                        for (int i = 0; i < str.Length; i++)
                        {
                            if (str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u')
                            {
                                nguyenam++;
                            }
                            else if (str[i] >= 'a' && str[i] <= 'z')
                            {
                                phuam++;
                            }
                        }

                        Console.WriteLine("So nguyen am co: " + nguyenam);
                        Console.WriteLine("So phu am co: " + phuam);
                        break;

                    case 4:
                        Console.WriteLine("Nhap so:");
                        //hàm int.Parse() để chuyển đổi chuỗi nhập vào thành một số nguyên và lưu trữ nó trong biến n
                        int n = int.Parse(Console.ReadLine());

                        int sum = 0;
                        while (n > 0)
                        {
                            sum += n % 10;
                            n /= 10;
                        }

                        Console.WriteLine("Tong cac chu so ban vua nhap: " + sum);
                        break;
                    case 5:
                        Console.Write("Nhap so phan tu cua mang: ");
                        int a = int.Parse(Console.ReadLine());
                        int[] arr = new int[a];
                        for (int i = 0; i < a; i++)
                        {
                            Console.Write("Nhap phan tu thu {0}: ", i + 1);
                            arr[i] = int.Parse(Console.ReadLine());
                        }
                        Array.Sort(arr);
                        Array.Reverse(arr);
                        Console.Write("Mang sau khi sap xep giam dan la: ");
                        for (int i = 0; i < a; i++)
                        {
                            Console.Write(arr[i] + " ");
                        }

                        Console.WriteLine();
                        break;
                    case 6:

                        Console.Write("Hay nhap so: ");
                        int b = int.Parse(Console.ReadLine());
                        double giaithua = 1;
                        for (int i = 2; i <= b; i++)
                        {
                            giaithua *= i;
                        }
                        Console.WriteLine("Giai thua {0} là {1}", b, giaithua);
                        break;
                    case 7:
                        ThongTin tt = new ThongTin();
                        Console.Write("Nhap hp ten: ");
                        tt.HoTen = Console.ReadLine();
                        Console.Write("Nhap nam sinh: ");
                        tt.NamSinh = Console.ReadLine();
                        Console.Write("Nhap gioi tinh: ");
                        tt.GioiTinh = Console.ReadLine();
                        Console.Write("Nhap que quan: ");
                        tt.QueQuan = Console.ReadLine();
                        Console.Write("Nha[ so CCCD: ");
                        tt.SoCCCD = Console.ReadLine();

                        Console.WriteLine("\nThong tin noi dung can cuoc:");
                        Console.WriteLine("Họ tên: " + tt.HoTen);
                        Console.WriteLine("Ngày sinh: " + tt.NamSinh);
                        Console.WriteLine("Ngày sinh: " + tt.GioiTinh);
                        Console.WriteLine("Quê quán: " + tt.QueQuan);
                        Console.WriteLine("Số CMND: " + tt.SoCCCD);
                        break;
                    /*case 8:
                        int d;
                        Console.Write("Nhap so: ");
                        d = int.Parse(Console.ReadLine());
                        int[] f = new int[d];
                        f[0] = 0;
                        f[1] = 1;
                        for (int i = 2; i < d; i++)
                        {
                            f[i] = f[i - 1] + f[i - 2];
                        }
                        Console.Write("Fibonacci " + d + " so: ");
                        for (int i = 0; i < d; i++)
                        {
                            Console.Write(f[i] + " ");
                        }
                        break;*/
                    case 0:
                        Console.WriteLine("\nThoat chuong trinh!");
                        return;
                    default:
                        Console.WriteLine("Ban nhap sai !! Vui long nhap lai.");
                        break;
                };
            } while (chon != 8);
        }
    }
}
